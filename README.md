Author: Meghan Riehl, mriehl5@uoregon.edu
Description:

   Basic webserver functionality, displays some pages with proper requests or a 404 or 403 error for other requests. 
	-Proper requests: trivia.html, trivia.css
	-Forbidden(403): ~, //, ..
	-Not found: Anything else or the files for trivia.html or trivia.css are not located
